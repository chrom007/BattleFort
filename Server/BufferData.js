class BufferData {
	constructor(data, align) {
		this.buffer = new Buffer(data);
		this.size = this.buffer.length;
		this.align = align - 1;
		this.cursor = 0;

		//console.log(this.buffer);
	}

	get data() {
		return this.buffer;
	}

	seek(align) {
		this.cursor = align;
	}

	setAlign(align) {
		this.align = align;
	}

	load(data) {
		this.buffer = Buffer.from(data);
		this.size = this.buffer.length;
		this.cursor = 0;
	}

	read_bool() {
		var cell = this.buffer.readUInt8(this.cursor);
		this.cursor += 1 + this.align;
		return (cell ? true : false );
	}

	read_s8() {
		var cell = this.buffer.readInt8(this.cursor);
		this.cursor += 1 + this.align;
		return cell;
	}

	read_s16() {
		var cell = this.buffer.readInt16LE(this.cursor);
		this.cursor += 2 + this.align;
		return cell;
	}

	read_s32() {
		var cell = this.buffer.readInt32LE(this.cursor);
		this.cursor += 4 + this.align;
		return cell;
	}

	write_u8(data) {
		this.buffer.writeUInt8(data, this.cursor);
		this.cursor += 1 + this.align;
	}

	read_u8() {
		var cell = this.buffer.readUInt8(this.cursor);
		this.cursor += 1 + this.align;
		return cell;
	}

	read_u16() {
		var cell = this.buffer.readUInt16LE(this.cursor);
		this.cursor += 2 + this.align;
		return cell;
	}

	read_u32() {
		var cell = this.buffer.readUInt32LE(this.cursor);
		this.cursor += 4 + this.align;
		return cell;
	}

	read_f32() {
		var cell = this.buffer.readFloatLE(this.cursor);
		this.cursor += 4 + this.align;
		return cell;
	}

	read_string() {
		var str = "";

		for(var c = this.cursor; c < this.size; c++) {
			if (this.buffer[c] == 0) break;

			str += String.fromCharCode(this.buffer[c]);
			this.cursor++;
		}

		this.cursor += this.align + 1;
		return str;
	}
}

module.exports = BufferData;