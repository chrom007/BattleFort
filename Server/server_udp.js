// BattleFort - server
// Created 30.04.18
// By @CHROM

const Client = require("./Client.js");
const BufferData = require("./BufferData.js");
const dgram = require('dgram');
const net = require("net");
const PORT = 6464;
var HOST = '0.0.0.0';

var buffer = new BufferData([], 1);
var clients = [];
var next_client = 1;


var server = dgram.createSocket('udp4');

server.on('listening', () => {
    var address = server.address();
    console.log('UDP Server listening on ' + address.address + ":" + address.port);
});

server.on('message', (message, remote) => {
    console.log(remote.address + ':' + remote.port +' - ' + message);
});

server.bind(PORT, HOST);

/*
var server = net.createServer((socket) => {
	console.log("Socket connected! IP: " + socket.remoteAddress);
	//socket.setEncoding("utf8");

	var client = new Client(socket);
	client.init(next_client++, clients);
	clients.push(client);

	socket.on("data", (data) => {
		buffer.load(data);
		var command = buffer.read_u8();

		switch(command) {
			case 10: {
				var uid = buffer.read_u8();
				var x = buffer.read_u16();
				var y = buffer.read_u16();
				console.log(`Player ${uid} | X: ${x} | Y: ${y}`);

				for(var c of clients) {
					c.send(data);
				}

				break;
			}
			default: {
				console.log("Unknown command: " + command);
			}
		}
	});

	socket.on("close", (data) => {
		console.log("Socket closed!");
	});
});

server.listen(PORT);

console.log("Server started! Port: " + PORT);*/

process.on('uncaughtException', function (err) {
    console.log(err.stack);
});