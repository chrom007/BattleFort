const BufferData = require("./BufferData.js");

class Client {
	constructor(socket) {
		this.socket = socket;
		this.ip = socket.remoteAddress;
		this.id = null;
		this.ticks = 0;

		this.socket.setNoDelay(true);
		console.log("User connected!");

		//setInterval(() => this.ticker(), 1000);
	}

	ticker() {
		if (this.ticks > 0) {
			console.log(`User ${this.id} ticks: ${this.ticks}`);
			this.ticks = 0;
		}
	}

	init(id, clients) {
		var buff = Buffer.allocUnsafe(10);

		// Send UID
		buff.fill(0);
		buff.writeUInt8(1, 0);
		buff.writeUInt8(id, 1);
		this.send(buff);
		this.id = id;

		// Send new player
		buff.fill(0);
		buff.writeUInt8(2, 0);
		buff.writeUInt8(id, 1);
		for(var c of clients) {
			c.send(buff);
		}

		// Send currenct players
		for(var c of clients) {
			buff.fill(0);
			buff.writeUInt8(2, 0);
			buff.writeUInt8(c.id, 1);
			this.send(buff);
		}
	}

	send(buffer) {
		this.socket.write(buffer);
		this.ticks += 1;
	}

	close() {
		this.socket.end();
	}
}

module.exports = Client;