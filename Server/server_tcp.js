// BattleFort - server
// Created 30.04.18
// By @CHROM

const Client = require("./Client.js");
const BufferData = require("./BufferData.js");
const net = require("net");
const PORT = 6464;

var buffer = new BufferData([], 1);
var clients = [];
var next_client = 1;
var ticks = 0;

setInterval(function(){
	if (ticks > 0)
		console.log("Ticks: " + ticks);
	ticks = 0;
}, 1000);

var server = net.createServer((socket) => {
	console.log("Socket connected! IP: " + socket.remoteAddress);
	//socket.setEncoding("utf8");

	var client = new Client(socket);
	client.init(next_client++, clients);
	clients.push(client);

	socket.on("data", (data) => {
		buffer.load(data);
		var command = buffer.read_u8();
		ticks += 1;

		switch(command) {
			case 10: {
				var uid = buffer.read_u8();
				var x = buffer.read_u16();
				var y = buffer.read_u16();
				//console.log(`Player ${uid} | X: ${x} | Y: ${y}`);

				for(var c of clients) {
					if (c.id != uid)
						c.send(data);
				};

				break;
			}
			default: {
				console.log("Unknown command: " + command);
			}
		}
	});

	socket.on("close", (data) => {
		console.log("Socket closed!");
	});
});

server.listen(PORT);

console.log("Server started! Port: " + PORT);

process.on('uncaughtException', function (err) {
    console.log(err.stack);
});