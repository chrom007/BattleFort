/// NetSyncPlayer() - Synchronize player X, Y positions

if (self.uid != noone) {
    buffer_seek(o_client.buffer, buffer_seek_start, 0);
    buffer_write(o_client.buffer, buffer_u8, 10);
    buffer_write(o_client.buffer, buffer_u8, self.uid);
    buffer_write(o_client.buffer, buffer_u16, self.x);
    buffer_write(o_client.buffer, buffer_u16, self.y);
    buffer_write(o_client.buffer, buffer_u16, self.image_angle);
    network_send_raw(o_client.socket, o_client.buffer, buffer_get_size(o_client.buffer));
}
